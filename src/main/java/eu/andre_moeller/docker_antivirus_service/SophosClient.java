package eu.andre_moeller.docker_antivirus_service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.FileOutputStream;

public class SophosClient {

    public String lastUpdate() {
        return "today";
    }

    public String scan(InputStream is, String filename) throws IOException {

        File file = new File(filename);
        OutputStream os = new FileOutputStream(file);
        String virus_info = "false";
        try {
            copyStream(is, os);

            String output = executeCommand("savscan -ss " + file.getAbsolutePath());
            file.delete();

            String[] lines = output.split("\n");
            for (String line : lines){
                if (line.contains(">>> Virus '")) virus_info= "Virus '" + line.split("'")[1] + "' in " + filename;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return virus_info;
    }

    public String status() {
        String output = executeCommand("savscan -v");

        return output;
    }


    private static void copyStream(InputStream input, OutputStream output)
            throws IOException
    {
        byte[] buffer = new byte[4096];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1)
        {
            output.write(buffer, 0, bytesRead);
        }
    }

    private String executeCommand(String command) {

        StringBuffer output = new StringBuffer();

        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            while ((line = reader.readLine())!= null) {
                output.append(line + "\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return output.toString();

    }

}