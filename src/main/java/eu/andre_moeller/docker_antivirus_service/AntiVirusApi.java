package eu.andre_moeller.docker_antivirus_service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class AntiVirusApi {


    /**
     * @return status.
     */
    @RequestMapping("/")
    public String status() throws IOException {
        
        SophosClient savclient = new SophosClient();
        String status = savclient.status();
        return status;
        
    }

    /**
     * @return av scan result
     */
    @RequestMapping(value = "/scan", method = RequestMethod.POST)
    public @ResponseBody
    String handleFileUpload(@RequestParam("name") String name,
                            @RequestParam("file") MultipartFile file) throws IOException {
        if (!file.isEmpty()) {
            SophosClient savclient = new SophosClient();
            String status = savclient.scan(file.getInputStream(), name);
            return "virus detection: " + status + " \n";
        } else throw new IllegalArgumentException("empty file");
    }
}